import PIL.Image as pim
import pickle
from bcdr import utils

def getimages(rows):
    imgs = []
    for a,b in [utils.extract_roi(i, 30, True) for i in rows]:
        imgs.append(b)
    return imgs

def resizeandsave(imgs, size, path):
    i = 0
    for a in imgs:
        a.thumbnail(size, pim.ANTIALIAS)
        a.save(path + str(i) + ".JPEG", "JPEG")
        i += 1

def getoversample(imgs):
    overimgs = []
    for a in imgs:
        oversamples = utils.oversample(a)
        overimgs += [oversamples[0]]
        for b in range(1, 8):
            im = pim.fromarray(oversamples[b])
            overimgs += [im]
    return overimgs

def getAnnotations(rows, path):
    filenames = ""
    for a in range(len(rows)):
        if rows[a]['classification'] == 'Benign':
            for b in range(8):
                filenames += path + str((a * 8) + b) + ".JPEG 0\n"
        else:
            for b in range(8):
       	       	filenames += path + str((a * 8) + b) + ".JPEG 1\n"
    return filenames

def write_file(filename, filelist):
    f = open(filename, 'w')
    f.write(filelist)
    f.close()

def extract():
    with open('setup.pkl') as f:
        train_rows, valid_rows, test_rows = pickle.load(f)
    imgstrain = getimages(train_rows)
    imgsvalid = getimages(valid_rows)
    imgstest = getimages(test_rows)
    size = 150, 150
    
    resizeandsave(getoversample(imgstrain), size, "train/")
    resizeandsave(getoversample(imgsvalid), size, "val/")
    resizeandsave(getoversample(imgstest), size, "test/")

    train = getAnnotations(train_rows, "train/")
    valid = getAnnotations(valid_rows, "val/")
    test = getAnnotations(test_rows, "test/")
    
    write_file('train.txt', train)
    write_file('val.txt', valid)
    write_file('test.txt', test)
    
extract()
