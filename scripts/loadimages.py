import PIL.Image as pim
import pickle
import numpy as np
import matplotlib.image as mpimg
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
from pylearn2.utils import serial
from pylearn2.utils import string_utils
from pylearn2.datasets import preprocessing

def get_images(path):
    lines = [line.strip() for line in open(path)]
    elements = [b.split() for b in lines]
    X = []
    y = []
    for row in elements:
        X.append(row[0])
        y.append(float(row[-1]))
    #X = np.asarray(X)
    y = np.asarray(y)
    y = y.reshape(y.shape[0], 1)
    Ximg = []
    for imagepath in X:
        Ximg.append(mpimg.imread(imagepath))
    topo_view = np.asarray(Ximg)
    m, r, c = topo_view.shape
    topo_view = topo_view.reshape(m, r, c, 1)
    return DenseDesignMatrix(topo_view=topo_view, X=None, y=y, view_converter=None)

def preprocess(dataset, which_set):
    pipeline = preprocessing.Pipeline()
    pipeline.items.append(preprocessing.GlobalContrastNormalization(use_std=True))
    pipeline.items.append(preprocessing.LeCunLCN((150, 150), kernel_size=11))
    dataset.apply_preprocessor(preprocessor=pipeline, can_fit=True)
    data_dir = string_utils.preprocess('${PYLEARN2_DATA_PATH}'+'/custom_dataset')
    output_dir = data_dir + "/" + which_set
    serial.mkdir(output_dir)
    return dataset.get_data(), output_dir

def normalize_image(image):
    imagearray = np.asarray(image)
    imagearray = imagearray.copy()
    minval = imagearray.min()
    maxval = imagearray.max()
    if minval != maxval:
        imagearray -= minval
        imagearray *= (255.0 / (maxval - minval))
    return imagearray

def print_images(output_dir, X):
    print "nothing"
    i = 0
    for image in X:
        img = pim.fromarray(normalize_image(image).reshape((150,150)).astype(np.uint8))
        print np.asarray(img)
        img.save(output_dir + "/" + str(i) + ".JPEG")
        i += 1

def orchestration():
    traindataset = get_images("train.txt")
    valdataset = get_images("val.txt")
    newdatatraining, output_dir_train = preprocess(traindataset, "train")
    newdataval, output_dir_val = preprocess(valdataset, "val")
    print_images(output_dir_train, newdatatraining[0])
    print_images(output_dir_val, newdataval[0])
    print newdataval[0][1]
    print len(newdataval[0])

orchestration()
